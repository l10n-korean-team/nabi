<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd" [

<!--

Process this file with an XSLT processor: `xsltproc \
-''-nonet /usr/share/sgml/docbook/stylesheet/xsl/nwalsh/\
manpages/docbook.xsl manpage.dbk'.  A manual page
<package>.<section> will be generated.  You may view the
manual page with: nroff -man <package>.<section> | less'.  A
typical entry in a Makefile or Makefile.am is:

DB2MAN=/usr/share/sgml/docbook/stylesheet/xsl/nwalsh/\
manpages/docbook.xsl
XP=xsltproc -''-nonet

manpage.1: manpage.dbk
        $(XP) $(DB2MAN) $<
    
The xsltproc binary is found in the xsltproc package.  The
XSL files are in docbook-xsl.  Please remember that if you
create the nroff version in one of the debian/rules file
targets (such as build), you will need to include xsltproc
and docbook-xsl in your Build-Depends control field.

-->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "<firstname>Changwoo</firstname>">
  <!ENTITY dhsurname   "<surname>Ryu</surname>">
  <!-- Please adjust the date whenever revising the manpage. -->
  <!ENTITY dhdate      "<date>September 13, 2003</date>">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1). -->
  <!ENTITY dhsection   "<manvolnum>1</manvolnum>">
  <!ENTITY dhemail     "<email>cwryu@debian.org</email>">
  <!ENTITY dhusername  "Changwoo Ryu">
  <!ENTITY dhucpackage "<refentrytitle>NABI</refentrytitle>">
  <!ENTITY dhpackage   "nabi">

  <!ENTITY debian      "<productname>Debian</productname>">
  <!ENTITY gnu         "<acronym>GNU</acronym>">
  <!ENTITY gpl         "&gnu; <acronym>GPL</acronym>">
]>

<refentry>
  <refentryinfo>
    <address>
      &dhemail;
    </address>
    <author>
      &dhfirstname;
      &dhsurname;
    </author>
    <copyright>
      <year>2003</year>
      <holder>&dhusername;</holder>
    </copyright>
    &dhdate;
  </refentryinfo>
  <refmeta>
    &dhucpackage;

    &dhsection;
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>

    <refpurpose>X Input Method Server for Korean text input</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
      <arg><option>OPTIONS</option></arg>
    </cmdsynopsis>
  </refsynopsisdiv>
  <refsect1>
    <title>DESCRIPTION</title>

    <para>This manual page documents briefly the
      <command>nabi</command> command.</para>

    <para>This manual page was written for the &debian; distribution
      because the original program does not have a manual page.</para>

    <para><command>&dhpackage;</command> is an X Input Method (XIM)
      server for Korean text (Hangul) input.  Additionally it shows
      the status of "imhangul" GTK+ input module in the notification
      area.</para>

  </refsect1>
  <refsect1>
    <title>OPTIONS</title>

    <variablelist>
      <varlistentry>
        <term><option>-s</option>
          <option>--status-only</option>
        </term>
        <listitem>
          <para>Doesn't be an input method.  Just display the Hangul
          input status in the system tray.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--sm-client-id=ID</option>
        </term>
        <listitem>
          <para>Specifies the session management ID.  This option is
          for session manager software, not for user typing.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>--xim-name=NAME</option>
        </term>
        <listitem>
          <para>Specifies the name of nabi's XIM service ("nabi" when
          it's not specified).  Useful for debugging.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term><option>-d LEVEL</option>
        </term>
        <listitem>
          <para>Set the debugging verborsity level.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
  <refsect1>
    <title>SEE ALSO</title>

    <para>See /usr/share/doc/nabi/README file for more
    information</para>
  </refsect1>
  <refsect1>
    <title>AUTHOR</title>

    <para>This manual page was written by &dhusername; &dhemail; for
      the &debian; system (but may be used by others). Permission is
      granted to copy, distribute and/or modify this document under
      the terms of the &gnu; General Public License, Version 2 any
      later version published by the Free Software Foundation.
    </para>
    <para>
      On Debian systems, the complete text of the GNU General Public
      License can be found in /usr/share/common-licenses/GPL.
    </para>

  </refsect1>
</refentry>
